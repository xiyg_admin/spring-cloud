package cn.danti.services.controller;

import cn.danti.services.service.ProductService;
import cn.danti.services.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RefreshScope
public class ProductController {

    @Autowired
    private ProductService productService;

    @Value("${version}")
    String version;

    @RequestMapping("/products")
    public Object products(Model m){
        List<Product> list= productService.products();
        m.addAttribute("ps",list);
        m.addAttribute("version",version);
        return "products";
    }
}
