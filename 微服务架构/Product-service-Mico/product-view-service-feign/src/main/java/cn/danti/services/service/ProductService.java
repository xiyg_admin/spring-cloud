package cn.danti.services.service;

import cn.danti.services.feign.ProductClientFeign;
import cn.danti.services.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductClientFeign productClientFeign;

    public List<Product> products(){
       return  productClientFeign.listProdcuts();
    }
}
