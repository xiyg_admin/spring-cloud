package cn.danti.services.ervice;

import cn.danti.services.pojo.Product;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    @Value("${server.port}")
    private int port;

    public List<Product>  listProduct(){
        List<Product> list = new ArrayList<>();
        list.add(new Product(1,"port"+ port,120));
        list.add(new Product(2,"port"+ port,121));
        list.add(new Product(3,"port"+ port,122));
        list.add(new Product(4,"port"+ port,123));
        return list;
    }

}
