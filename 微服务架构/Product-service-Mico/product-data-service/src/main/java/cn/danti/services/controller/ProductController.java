package cn.danti.services.controller;

import cn.danti.services.ervice.ProductService;
import cn.danti.services.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/products")
    public Object products(Model m){
        List<Product> list = productService.listProduct();
        return list;
    }

}
