package cn.danti.services;

import cn.hutool.core.util.NetUtil;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * 注册中心
 *
 */

@SpringBootApplication
@EnableEurekaServer
public class eurekaserviceApplication
{
    public static void main( String[] args )
    {
        int port = 8761;
        if (!NetUtil.isUsableLocalPort(port)) {
            System.out.printf("端口号不可用：%d",port);
            System.exit(1);
        }
        new SpringApplicationBuilder(eurekaserviceApplication.class).properties("server.port="+port).run(args);
    }
}
