package cn.danti.services.controller;

import cn.danti.services.pojo.Product;
import cn.danti.services.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.jws.WebParam;
import java.util.List;

@Controller
public class ProductController {

    @Autowired
    public ProductService productService;

    @RequestMapping("/products")
    public Object products(Model m){
       List<Product> list =  productService.listProduct();
       m.addAttribute("ps",list);
       return "products";
    }
}
