package cn.danti.services.service;

import cn.danti.services.pojo.Product;
import cn.danti.services.ribbon.RibbonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    public RibbonClient ribbonClient;

    public List<Product> listProduct(){
       return  ribbonClient.listproducts();
    }
}
