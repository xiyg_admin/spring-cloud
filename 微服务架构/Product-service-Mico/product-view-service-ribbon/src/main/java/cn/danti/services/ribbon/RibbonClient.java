package cn.danti.services.ribbon;

import cn.danti.services.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class RibbonClient {

    @Autowired
    public RestTemplate restTemplate;

    public List<Product> listproducts(){

     return restTemplate.getForObject("http://PRODUCT-DATA-SERVICE/products", List.class);

    }
}
