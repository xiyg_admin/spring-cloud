package cn.danti.service.productservice.controller;

import cn.danti.service.productservice.pojo.Product;
import cn.danti.service.productservice.service.ProduceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class ProductProduct {
    @Autowired
    public ProduceService produceService;

    
    @RequestMapping("/products")
    public Object products(Model m){
        List<Product> productList = produceService.listProducts();
        m.addAttribute("ps",productList);
        return "products";
    }
}
