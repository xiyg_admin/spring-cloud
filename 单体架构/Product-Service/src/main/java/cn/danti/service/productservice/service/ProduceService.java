package cn.danti.service.productservice.service;

import cn.danti.service.productservice.pojo.Product;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProduceService {

    public List<Product> listProducts(){
        List<Product> list = new ArrayList<>();
        list.add(new Product(1,"name1",120));
        list.add(new Product(2,"name1",120));
        list.add(new Product(3,"name1",120));
        return list;
    }
}
