# SpringCloud
下面是spring官网的spring cloud微服务架构图：
![输入图片说明](images/20190305130804158.png)
![输入图片说明](images/20190305130055240.png)
![输入图片说明](images/20191222122813549.png)
![输入图片说明](images/20190305125527818.png)
#### 介绍
在讲解Springcloud 之前，我们先讲一讲单体架构系统。 所谓的单体架构就是所有功能，都放在一个应用里。
比如后面要讲的一个单体产品服务应用，提供数据和视图都在一个springboot里。

单体架构系统有其好处，如便于开发，测试，部署也很方便，直接打成一个 jar 或者 war, 就什么都好了。

不过单体架构也有其弊端，最主要体现在高访问，高并发的上限是固定的。 比如一个单体架构，能够承受 1000次访问/秒。 但是访问量达到 2000次/秒的时候，就会非常卡顿，严重影响业务，并且仅仅依靠单体架构本身，很难突破这个瓶颈了。
既然单体架构会有性能上的瓶颈，那么总要解决呀。 解决办法通常就是采用分布式和集群来做。
可是分布式和集群分别是什么意思呢？
这个就不是一两句话能够说的清楚的啦。。。。 站长又不愿意直接丢一大堆概念给同学们，让没有接触过的同学们更加云里雾里，所以站长会先讲解单体架构，然后对这个单体架构进行逐步改造，最后让其变成一个分布式和集群系统，通过参与这个过程，大家才能够感性地接触和认识到底什么是分布式和集群。
SpringCloud 就是一套工具，帮助大家很容易地搭建出这么一个 集群和分布式的架子出来。

接下来的课程，站长就会从一个简单的单站系统开始，然后一步一步地把它改造成分布式+集群的系统。 在改造的过程中，就会逐步引入服务注册中心 Eureka, 客户端Ribbon,Feigh, 断路保护 Hystrix, 配置服务，消息总线等等概念和用法。
按部就班地跟着站长走一遍，差不多SpringCloud 的初步运用就能掌握啦。
SpringCloud 的内容不少，将按照如下节奏展开

- [springcloud 单体架构例子](https://how2j.cn/k/springcloud/springcloud-single/2036.html)
- [springcloud 分布式和集群](https://how2j.cn/k/springcloud/springcloud-eureka-server/2038.html)
- [springcloud 父子项目](https://how2j.cn/k/springcloud/springcloud-parent-child/2053.html)
- [springcloud 服务注册中心](https://how2j.cn/k/springcloud/springcloud-eureka-server/2038.html)
- [springcloud 注册数据微服务](https://how2j.cn/k/springcloud/springcloud-eureka-client/2039.html)
- [springcloud 视图微服务-Ribbon](https://how2j.cn/k/springcloud/springcloud-ribbon/2040.html)
- [springcloud 视图微服务-Feign](https://how2j.cn/k/springcloud/springcloud-feign/2041.html)
- [springcloud 服务链路追踪](https://how2j.cn/k/springcloud/springcloud-feign/2041.html)
- [springcloud 配置服务器](https://how2j.cn/k/springcloud/springcloud-config-server/2047.html)
- [springcloud 配置客户端](https://how2j.cn/k/springcloud/springcloud-config-client/2048.html)
- [springcloud 消息总线Bus](https://how2j.cn/k/springcloud/springcloud-bus/2049.html)
- [springcloud 断路器 Hystrix](https://how2j.cn/k/springcloud/springcloud-hystrix/2042.html)
- [springcloud 断路器监控](https://how2j.cn/k/springcloud/springcloud-dashboard/2043.html)
- [springcloud 断路器聚合监控](https://how2j.cn/k/springcloud/springcloud-turbine/2044.html)
- [springcloud 网关 Zuul](https://how2j.cn/k/springcloud/springcloud-zuul/2045.html)
- [springcloud 端口号总结](https://how2j.cn/k/springcloud/springcloud-ports/2054.html)

